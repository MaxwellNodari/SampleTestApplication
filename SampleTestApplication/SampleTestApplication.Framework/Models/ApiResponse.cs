﻿namespace SampleTestApplication.Framework.Models
{
    public class ApiResponse
    {
        public string Status { get; set; }
        public string? Code { get; set; }
        public string Message { get; set; }
        public List<ArticleModel> Articles { get; set; }
        public int TotalResults { get; set; }
    }
}
