﻿using Newtonsoft.Json;

namespace SampleTestApplication.Framework.Models
{
    public class ArticleModel
    {
        [JsonProperty("author", NullValueHandling = NullValueHandling.Ignore)]
        public string Author { get; set; }
        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }
        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }
        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }
        [JsonProperty("urlToImage", NullValueHandling = NullValueHandling.Ignore)]
        public string UrlToImage { get; set; }
    }
}
