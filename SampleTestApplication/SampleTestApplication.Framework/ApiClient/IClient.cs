﻿using SampleTestApplication.Framework.Models;

namespace SampleTestApplication.Framework.ApiClient
{
    public interface IClient
    {
        Task<ApiResponse> GetEverythingAsync();
    }
}
