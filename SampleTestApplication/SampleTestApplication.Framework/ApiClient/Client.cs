﻿
using Newtonsoft.Json;
using RestSharp;
using SampleTestApplication.Framework.Models;
using System.Collections.Generic;

namespace SampleTestApplication.Framework.ApiClient
{
    public class Client : IClient
    {
        private readonly string _apiKey;
        private readonly RestClient _restClient;
        public Client(string apiKey)
        {
            _apiKey = apiKey;
            _restClient = new RestClient("https://newsapi.org/v2/");
        }

        public async Task<ApiResponse> GetEverythingAsync()
        {
            ApiResponse apiResponse = new();
            RestRequest request = new RestRequest("everything", Method.Get);
            var response = await GetResponseAsync(request);
            apiResponse = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
            return apiResponse;
        }
        private async Task<RestResponse> GetResponseAsync(RestRequest request)
        {
            request.AddHeader("user-agent", "News-API-csharp/0.1");
            request.AddHeader("x-api-key", _apiKey);
            request.AddParameter("q", "News"); 
                request.AddParameter("from", "2023-03-21");
            RestResponse response = _restClient.Execute(request);
            if (!response.IsSuccessful)
            {
                throw new Exception(response.Content);
            }

            return response;
        }
    }
}
