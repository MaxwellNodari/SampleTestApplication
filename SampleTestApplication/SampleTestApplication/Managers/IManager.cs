﻿namespace SampleTestApplication.Console.Managers
{
    internal interface IManager
    {
        Task StartAsync();
    }
}
