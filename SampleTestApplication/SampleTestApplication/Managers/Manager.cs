﻿using SampleTestApplication.Framework.ApiClient;
using System;
namespace SampleTestApplication.Console.Managers
{
    public class Manager : IManager
    {
        private readonly IClient _client;

        public Manager(IClient client)
        {
            _client = client;
        }

        public async Task StartAsync()
        {
            var articlesResponse = await _client.GetEverythingAsync();

            foreach (var article in articlesResponse.Articles)
            {
                System.Console.WriteLine($"Title: {article.Title}");
                System.Console.WriteLine($"Author:{article.Author}");
                System.Console.WriteLine($"Description: {article.Description}");
                System.Console.WriteLine($"Url: {article.Url}\n");
            }
        }
    }
}
