﻿using Microsoft.Extensions.DependencyInjection;
using SampleTestApplication.Console.Managers;
using SampleTestApplication.Framework.ApiClient;

namespace SampleTestApplication.Console
{
    internal static class ServiceProviderBuilder
    {
        internal static IServiceProvider Build()
        {
            IServiceCollection services = new ServiceCollection();
            services.AddScoped<IManager, Manager>();
            services.AddScoped<IClient>(c => new Client("36afaee38e284f8488922f6ce81a03a3"));
            return services.BuildServiceProvider();
        }
    }
}
