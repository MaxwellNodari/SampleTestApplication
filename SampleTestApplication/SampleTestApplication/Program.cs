﻿using Microsoft.Extensions.DependencyInjection;
using SampleTestApplication.Console;
using SampleTestApplication.Console.Managers;
// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

ServiceProviderBuilder
    .Build()
    .GetRequiredService<IManager>()
    .StartAsync();
